package com.itau.jogo.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.jogo.model.Jogada;
import com.itau.jogo.model.Jogo;
import com.itau.jogo.model.Valor;


@RestController
public class JogoController {
	
	Jogo jogo;
	private Valor[] valores = {Valor.X, Valor.O};
	RestTemplate restTemplate = new RestTemplate();
	String msg;
	String rabbit;
	
	@RequestMapping(path="/jogo/jogar", method=RequestMethod.POST)
	public ResponseEntity<?> jogar(@RequestBody Jogada jogada){
		
		Valor valorDaVez = valores[jogo.getJogadorAtivo()];
		
		jogada.setValor(valorDaVez);
		
		boolean sucesso = restTemplate.postForObject("http://localhost:8083/tabuleiro/setCasa", jogada, boolean.class);
		
		boolean vitoria = restTemplate.getForObject("http://localhost:8083/tabuleiro/verificarVitoria", boolean.class);
		
		boolean velha = restTemplate.getForObject("http://localhost:8083/tabuleiro/verificarVelha", boolean.class);
		
		msg = "Jogada na posicao: " + jogada.x + ", " + jogada.y + " | Jogador: " + jogada.getValor();
		
		msg += " | Status: " + sucesso + " | Vitoria: " + vitoria + " | Velha: " + velha;
		
		jogo.jogar(sucesso, vitoria, velha);
		
		rabbit = restTemplate.postForObject("http://localhost:8080/", msg, String.class);
		
		return ResponseEntity.ok(jogo);
		
	}
	
	@RequestMapping(path="/jogo/status", method=RequestMethod.GET)
	public ResponseEntity<?> getJogo() {
		return ResponseEntity.ok(jogo);
	}
	
	
	@RequestMapping(path="/jogo/iniciar", method=RequestMethod.GET)
	public ResponseEntity<?> zerarTabuleiro() {
		jogo = new Jogo();
		ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8083/tabuleiro/vazio", String.class);
		msg = "Novo jogo | Data e hora: " + dataHoraJogo();
		rabbit = restTemplate.postForObject("http://localhost:8080/", msg, String.class);
		return response.ok(jogo);
	}
	
	public String dataHoraJogo() {		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
		Date date = new Date(); 
		String dataHora = dateFormat.format(date);
		return dataHora;
	}
	
}
