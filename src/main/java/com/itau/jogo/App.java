package com.itau.jogo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.itau.jogo.App;
import com.itau.jogo.controller.JogoController;

@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    	JogoController jogo = new JogoController();
    	System.out.println(jogo.dataHoraJogo());
    }
}
