package com.itau.jogo.model;

public class Jogada {
	private Valor valor;
	public int x;
	public int y;
	
	public Valor getValor() {
		return valor;
	}
	public void setValor(Valor valor) {
		this.valor = valor;
	}
}
