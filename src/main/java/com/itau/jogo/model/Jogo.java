package com.itau.jogo.model;

public class Jogo {
	//private Tabuleiro tabuleiro = new Tabuleiro();
    //private Valor[] valores = {Valor.X, Valor.O};

    private int jogadorAtivo;
    private boolean encerrado;
    private boolean vitoria;

    public Jogo(){
    	jogadorAtivo = 0;
        encerrado = false;
        vitoria = false;
    }

    public void jogar(boolean sucesso, boolean vitoria, boolean velha){
        /*if(encerrado){
            return;
        }*/

        //Valor valorDaVez = valores[jogadorAtivo];

      //  a linha abaixo irá para o JogoController, para chamar a API de tabuleiro e setar a casa
      //  boolean sucesso = tabuleiro.setCasa(x, y, valorDaVez);

        if(!sucesso){
            return;
        }
        
        //a linha abaixo irá para o JogoController, para chamar a API de tabuleiro e verificar se houve vitoria ou velha
        //vitoria = tabuleiro.verificarVitoria();
        //boolean velha = tabuleiro.verificarVelha();

        if(vitoria || velha){
            encerrado = true;
            return;
        }

        if(jogadorAtivo == 0){
            jogadorAtivo = 1;
        }else{
            jogadorAtivo = 0;
        }
    }

    // metodo devera ser transferido para o controller
    /*public String[][] getCasas(){
        return tabuleiro.getCasas();
    }*/

    public boolean isEncerrado(){
        return encerrado;
    }

    public boolean isVitoria(){
        return vitoria;
    }

    public int getJogadorAtivo(){
        return jogadorAtivo;
    }

}
